<?php

declare(strict_types=1);

return [
    'csrf' => [
        'csrf_key_name' => 'csrf_key',
        'csrf_key_length' => 32,
        'csrf_hash_name' => 'csrf_hash',
        'error_route_name' => 'error.index'
    ],
];
