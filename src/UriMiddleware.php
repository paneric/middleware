<?php

declare(strict_types=1);

namespace Paneric\Middleware;

use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Twig\Extension\TwigExtension;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Interfaces\RouteParserInterface;
use Twig\Environment as Twig;

class UriMiddleware implements MiddlewareInterface
{
    public function __construct(protected ContainerInterface $container)
    {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $request = $this->setCallbackUri($request);

        if ($this->container->has(Twig::class)) {
            $twig = $this->container->get(Twig::class);

            $twigExtension = new TwigExtension(
                $this->container->get(RouteParserInterface::class),
                $request->getUri(),
                $request->getAttribute('route_name'),
                $this->container->get('root_folder'),
                $this->container->get('base_url')
            );
            $twig->addExtension($twigExtension);
        }

        return $handler->handle($request);
    }

    private function setCallbackUri(ServerRequestInterface $request): ServerRequestInterface
    {
        if (!$this->container->has(SessionInterface::class)) {
            return $request;
        }

        $session = $this->container->get(SessionInterface::class);

        $previousUriState = $session->getData('uri_state');

        $currentUriPath = $request->getUri()->getPath();

        $mainRouteName = $this->container->get('main_route_name');

        if ($previousUriState === null) {
            $previousUriState = [
                'previous_uri_path' => '/',
                'current_uri_path' => '/',
                'previous_route_name' => $mainRouteName,
                'current_route_name' => $mainRouteName,
            ];
        }

        if ($previousUriState['current_uri_path'] !== $currentUriPath) {
            $uriState = [
                'previous_uri_path' => $previousUriState['current_uri_path'],
                'current_uri_path' => $currentUriPath,
            ];
        }

        if ($previousUriState['current_uri_path'] === $currentUriPath) {
            $uriState = $previousUriState;
        }

        $uriState['previous_route_name'] = $previousUriState['current_route_name'];
        $uriState['current_route_name'] = $request->getAttribute('route_name');

        $session->setData($uriState, 'uri_state');

        return $request->withAttribute(
            'previous_uri_path',
            $uriState['previous_uri_path']
        );
    }
}
