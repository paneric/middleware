<?php

declare(strict_types=1);

namespace Paneric\Middleware;

use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Slim\Exception\HttpUnauthorizedException;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class CSRFMiddleware  implements MiddlewareInterface
{
    public function __construct(
        protected SessionInterface $session,
        protected GuardInterface $guard,
        protected array $config
    ) {
    }

    /**
     * @throws HttpUnauthorizedException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request->getMethod() === 'GET') {
            return $handler->handle($request);
        }

        $registeredCsrf = $this->session->getData($this->config['csrf_key_name']);
        $requestParsedBody = $request->getParsedBody();

        if ($registeredCsrf === null || !isset($requestParsedBody[$this->config['csrf_hash_name']])) {
            throw new HttpUnauthorizedException(
                $request, 'CSRF: Unauthorized (error log available).'
            );
        }

        $validCSRFHash = $this->guard->verifyHash(
            $this->guard->hash($registeredCsrf),
            $requestParsedBody[$this->config['csrf_hash_name']]
        );

        if (!$validCSRFHash) {
            throw new HttpUnauthorizedException($request,
                'CSRF: Unauthorized (error log available).'
            );
        }

        return $handler->handle($request);
    }
}
