<?php

declare(strict_types=1);

namespace Paneric\Middleware;

use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AuthenticationMiddleware implements MiddlewareInterface
{
    public function __construct(protected SessionInterface $session)
    {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $authentication = $this->session->getData('authentication');

        if ($authentication === null) {
            $this->session->setData([],'authentication');
        }

        return $handler->handle($request);
    }
}
