<?php

declare(strict_types=1);

namespace Paneric\Middleware;

use Paneric\Slim\Exception\HttpInternalServerErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RouteMiddleware implements MiddlewareInterface
{
    public const ROUTE = '__route__';

    public function __construct(protected ContainerInterface $container)
    {
    }

    /**
     * @throws HttpInternalServerErrorException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $route = $request->getAttribute(self::ROUTE);

        $request = $request->withAttribute('route_name', $route->getName())
            ->withAttribute('route_groups', $route->getGroups())
            ->withAttribute('route_methods', $route->getGroups())
            ->withAttribute('route_arguments', $route->getArguments());

        if ($this->container->has('proxy_prefix')) {

            try {
                $proxyPrefix = $this->container->get('proxy_prefix');

                $request = $request->withAttribute('proxy_prefix', $proxyPrefix);

                $proxyPrefix = str_replace('/', '', $proxyPrefix);
                $templateNamespace = explode('-', $proxyPrefix);
                $prefix = end($templateNamespace);
                $proxyScope = str_replace('-' . $prefix, '', $proxyPrefix);

                $request = $request->withAttribute('proxy_scope', $proxyScope);

            } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
                throw new HttpInternalServerErrorException(
                    $request, 'DI: Container malfunction (error log available).'
                );
            }
        }

        return $handler->handle($request);
    }
}
